package com.JackPB.bot.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.JackPB.bot.model.TickerData;

public class CalculateTest {

	@InjectMocks
	private Calculate calculate;

	@Test
	public void diffPercentTest() {
		BigDecimal result = this.calculate.diffPercent(BigDecimal.valueOf(10), BigDecimal.valueOf(100));
		assertThat(result.toString()).isEqualTo("90.0");
		result = this.calculate.diffPercent(BigDecimal.valueOf(190), BigDecimal.valueOf(200));
		assertThat(result.toString()).isEqualTo("5.0");
		result = this.calculate.diffPercent(BigDecimal.valueOf(49), BigDecimal.valueOf(100));
		assertThat(result.toString()).isEqualTo("51.0");
	}

	@Test
	public void diffWithMarginTest() {
		TickerData tickerBuy = TickerData.builder().ask(BigDecimal.valueOf(99)).build();
		TickerData tickerSell = TickerData.builder().bid(BigDecimal.valueOf(100)).build();
		BigDecimal result = this.calculate.diffWithMargin(tickerBuy, tickerSell, BigDecimal.valueOf(2));
		assertThat(result.toString()).isEqualTo("-1.00");
		result = this.calculate.diffWithMargin(tickerBuy, tickerSell, BigDecimal.valueOf(0.5));
		assertThat(result.toString()).isEqualTo("0.50");

	}

}
