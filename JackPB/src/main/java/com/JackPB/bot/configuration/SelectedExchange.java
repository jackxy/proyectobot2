package com.JackPB.bot.configuration;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;

import java.util.ArrayList;
import java.util.List;

import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.BinanceSpecs;
import com.JackPB.bot.exchange.BitfinexSpecs;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.exchange.HitbtcSpecs;
import com.JackPB.bot.exchange.KucoinSpecs;

public class SelectedExchange {

	private static SelectedExchange selectedExchange;
	private ArrayList<ExchangeSpecs> exchanges = new ArrayList<ExchangeSpecs>();

	private SelectedExchange(List<ActiveExchange> activeExchanges) {
		activeExchanges.parallelStream().forEach(activeExchange -> {
			ExchangeSpecs newExchange = Match(activeExchange.getName().toLowerCase()).of(
					Case($("Bitfinex".toLowerCase()),
							new BitfinexSpecs(activeExchange.getApiKey(), activeExchange.getSecretKey(),
									activeExchange.getName(), activeExchange.getBuyFee(), activeExchange.getBuyFee())),
					Case($("Hitbtc".toLowerCase()),
							new HitbtcSpecs(activeExchange.getApiKey(), activeExchange.getSecretKey(),
									activeExchange.getName(), activeExchange.getBuyFee(), activeExchange.getBuyFee())),
					Case($("Kucoin".toLowerCase()),
							new KucoinSpecs(activeExchange.getApiKey(), activeExchange.getSecretKey(),
									activeExchange.getName(), activeExchange.getBuyFee(),
									activeExchange.getBuyFee(), activeExchange.getPassphrase())),
					Case($("Binance".toLowerCase()),
							new BinanceSpecs(activeExchange.getApiKey(), activeExchange.getSecretKey(),
									activeExchange.getName(), activeExchange.getBuyFee(), activeExchange.getBuyFee())));
			this.exchanges.add(newExchange);

		});

	}

	public static void reset() {
		selectedExchange = null;

	}

	public static SelectedExchange getSelectedExchange(List<ActiveExchange> activeExchanges) {
		if (selectedExchange == null) {
			selectedExchange = new SelectedExchange(activeExchanges);

		}
		return selectedExchange;

	}

	public static SelectedExchange getSelectedExchange() {
		return selectedExchange;
	}

	public ArrayList<ExchangeSpecs> getExchange() {
		return this.exchanges;

	}

}
