package com.JackPB.bot.exchange;

import java.math.BigDecimal;

import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.BitfinexExchange;

public class BitfinexSpecs extends ExchangeSpecs {

	public BitfinexSpecs(String apiKey, String secretKey, String exchangeName, BigDecimal buyfee, BigDecimal sellFee) {
		super(apiKey, secretKey, "jackxy@me.com", exchangeName, buyfee, sellFee);
		if (null != apiKey && null != secretKey) {
			setRealMode(true);

		}
	}

	public BitfinexSpecs() {
		super();
	}

	@Override

	public ExchangeSpecification getSetupedExchange() {
		ExchangeSpecification exSpec = new BitfinexExchange().getDefaultExchangeSpecification();
		if (super.getRealMode()) {

			exSpec.setApiKey(super.getApiKey());
			exSpec.setSecretKey(super.getSecretKey());

		}
		return exSpec;

	}
}
