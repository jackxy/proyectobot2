package com.JackPB.bot.exchange;

import java.math.BigDecimal;

import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.binance.BinanceExchange;

public class BinanceSpecs extends ExchangeSpecs {

	

	
	public BinanceSpecs(String apiKey, String secretKey, String exchangeName, BigDecimal buyfee,
			BigDecimal sellFee) {
		super(apiKey, secretKey, "ubifenix@yahoo.es", exchangeName, buyfee, sellFee);
		if (null != apiKey && null != secretKey) {
			setRealMode(true);
			}
	}
	
	public BinanceSpecs() {
		super();
		
		
		
	}

	
	@Override
	public ExchangeSpecification getSetupedExchange() {
		ExchangeSpecification exSpec = new BinanceExchange().getDefaultExchangeSpecification();
		if (super.getRealMode()) {
			exSpec.setUserName(super.getUserName());
			exSpec.setApiKey(super.getApiKey());
			exSpec.setSecretKey(super.getSecretKey());

		}
		return exSpec;

	}
}
