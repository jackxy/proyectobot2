package com.JackPB.bot.exchange;

import java.math.BigDecimal;

import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.hitbtc.v2.HitbtcExchange;



public class HitbtcSpecs extends ExchangeSpecs {

	public HitbtcSpecs(String apiKey, String secretKey, String exchangeName, BigDecimal buyfee, BigDecimal sellFee) {
		super(apiKey, secretKey, "Jackxy@me.com", exchangeName, buyfee, sellFee);
		if (null != apiKey && null != secretKey) {
			setRealMode(true);
		}
	}

	public HitbtcSpecs() {
		super();

	}

	@Override
	public ExchangeSpecification getSetupedExchange() {
		ExchangeSpecification exSpec = new HitbtcExchange().getDefaultExchangeSpecification();
		if (super.getRealMode()) {
			exSpec.setUserName(super.getUserName());
			exSpec.setApiKey(super.getApiKey());
			exSpec.setSecretKey(super.getSecretKey());

		}
		return exSpec;

	}

}
