package com.JackPB.bot.utils;

import java.math.BigDecimal;
import java.math.MathContext;

import org.springframework.stereotype.Component;

import com.JackPB.bot.model.TickerData;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Calculate {
	
	
	
	public static BigDecimal diffWithMargin(TickerData tickerBuy, TickerData tickerSell, 
			BigDecimal arbitrageFee) {
		BigDecimal diffPercent = findDiff(tickerBuy, tickerSell);
		BigDecimal arbitragePerDiffSubMarg = diffPercent.subtract(arbitrageFee);
		log.info("Procentaje de diferencia -> " + diffPercent + "%");
		log.info("Procentaje necesario -> " + arbitrageFee + "%");
		log.info("Procentaje restante -> " + arbitragePerDiffSubMarg + "%");
		return arbitragePerDiffSubMarg;
		
	}
	
	public static BigDecimal findDiff(TickerData lowAsk, TickerData highBid) {
		return diffPercent(lowAsk.getAsk(), highBid.getBid());
	}
	
	public static BigDecimal diffPercent(BigDecimal low, BigDecimal high) {
		MathContext mc = new MathContext(10);
		BigDecimal subtract = high.subtract(low);
		BigDecimal difference = subtract.divide(high, mc);
		BigDecimal diffPercent = difference.multiply(BigDecimal.valueOf(100));
		return diffPercent;
		
	}

	
	}


