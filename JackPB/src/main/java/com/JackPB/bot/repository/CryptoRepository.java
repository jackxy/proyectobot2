package com.JackPB.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.JackPB.bot.entity.Crypto;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Integer> {

}
