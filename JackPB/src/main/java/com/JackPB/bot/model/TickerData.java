package com.JackPB.bot.model;

import java.math.BigDecimal;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.CurrencyPair;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;



@Data
@ToString
@AllArgsConstructor
@Builder
public class TickerData {
	private CurrencyPair currencyPair;
	Exchange exchange;
	private BigDecimal bid;
	private BigDecimal ask;
	private BigDecimal last;
	

}


