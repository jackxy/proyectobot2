package com.JackPB.bot.model;

import java.math.BigDecimal;

public class TickerDataDTO extends TickerData {
	BigDecimal baseFund;
	BigDecimal counterFund;

	public TickerDataDTO(TickerData tickerData, BigDecimal baseFund, BigDecimal counterFund) {
		super(tickerData.getCurrencyPair(), tickerData.getExchange(), tickerData.getBid(), tickerData.getAsk(),
				tickerData.getLast());
		this.baseFund = baseFund;
		this.counterFund = counterFund;

	}

	public TickerDataDTO(TickerData tickerData) {
		super(tickerData.getCurrencyPair(), tickerData.getExchange(), tickerData.getBid(), tickerData.getAsk(),
				tickerData.getLast());
	}

	public BigDecimal getBaseFund() {
		return baseFund;

	}

	public void setBaseFund(BigDecimal baseFund) {
		this.baseFund = baseFund;
	}

	public BigDecimal getCounterFund() {
		return counterFund;

	}

	public void setCounterFund(BigDecimal counterFund) {
		this.counterFund = counterFund;
	}

	@Override
	public String toString() {
		return "TickerDataTrading{" + super.toString() + "baseFund=" + baseFund + ", counterFund=" + counterFund + '}';

	}

	public void add(TickerDataDTO tickerData) {
		// TODO Auto-generated method stub
		
	}

	

	}


