package com.JackPB.bot.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JackPB.bot.configuration.SelectedExchange;
import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.entity.Crypto;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.model.TickerDataDTO;
import com.JackPB.bot.repository.CryptoRepository;
import com.JackPB.bot.service.ArbitrageService;
import com.JackPB.bot.service.ExchangeService;
import com.JackPB.bot.service.TickerDataService;
import com.JackPB.bot.service.TradeService;

import io.vavr.Tuple2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ArbitrageServiceimpl implements ArbitrageService {

	@Autowired
	ExchangeService exchangeService;

	@Autowired
	CryptoRepository cryptorepository;
	@Autowired
	TickerDataService tickerDataService;
	@Autowired
	TradeService tradeservice;

	@Override

	public void botInit() {
		log.info("BOT INIT");
		List<ActiveExchange> activeExchanges = this.exchangeService.findByActiveExchange();
		ArrayList<ExchangeSpecs> selectedExchanges = SelectedExchange.getSelectedExchange(activeExchanges)
				.getExchange();
		ArrayList<ActivatedExchange> activatedExchanges = this.exchangeService.getActivatedExchanges(selectedExchanges);
		List<Crypto> cryptos = this.cryptorepository.findAll();
		if (cryptos.stream().anyMatch(crypto -> BooleanUtils.isTrue(crypto.getActive()))) {
			log.info("Bot en funcionamiento. Criptomonedas Activas.");
			cryptos.stream().filter(c -> !c.getCommonCode().equals("BTC") && BooleanUtils.isTrue(c.getActive()))
					.forEach(crypto -> {
						try {
							this.arbitrage(activatedExchanges, new CurrencyPair(crypto.getCurrencyPair()),
									BigDecimal.valueOf(crypto.getAmountValueBase()));
						} catch (Exception e) {
							log.error(e.getMessage());
						}

					});
			SelectedExchange.reset();

		} else {
			log.info("Bot parado. Cryptomonedas Desactivadas.");

		}
	}

	private void arbitrage(ArrayList<ActivatedExchange> activatedExchanges, CurrencyPair currencyPair,
			BigDecimal amountValueBase) {
		ArrayList<TickerDataDTO> tickersData = this.exchangeService.getAllTickerData(activatedExchanges, currencyPair,
				amountValueBase);
		Tuple2<TickerDataDTO, TickerDataDTO> buySellTickers = this.tickerDataService.findLowHighPrice(tickersData);
		if (this.tradeservice.canTrade(buySellTickers._1, buySellTickers._2)) {
			// TODO:TRADE
		}

	}
}
