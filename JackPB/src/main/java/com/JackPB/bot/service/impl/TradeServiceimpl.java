package com.JackPB.bot.service.impl;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.JackPB.bot.configuration.SelectedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.model.TickerDataDTO;
import com.JackPB.bot.service.TradeService;
import com.JackPB.bot.utils.Calculate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TradeServiceimpl implements TradeService {

	@Override
	public Boolean canTrade(TickerDataDTO tickerBuy, TickerDataDTO tickerSell) {
		if(canParallel(tickerBuy, tickerSell)) {
		// if (this.strategy(tickerBuy, tickerSell) && this.hasFunds(tickerBuy, tickerSell)) {

	return true;
		} else {
			return false;
		}
	}

	private Boolean canParallel(TickerDataDTO tickerBuy, TickerDataDTO tickerSell) {
		Boolean canTrade = false;
		CompletableFuture<Boolean> strategy = CompletableFuture.supplyAsync(() -> strategy(tickerBuy, tickerSell));
		CompletableFuture<Boolean> hasFunds = CompletableFuture.supplyAsync(() -> hasFunds(tickerBuy, tickerSell));
		CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(strategy, hasFunds);
		try {
			combinedFuture.get();
			canTrade = strategy.get() && hasFunds.get();
		} catch (InterruptedException | ExecutionException e) {
			log.error(e.getMessage(), tickerBuy.getCurrencyPair().toString());
		}

		return canTrade;
	}

	private boolean hasFunds(TickerDataDTO tickerBuy, TickerDataDTO tickerSell) {

		return true;
	}

	private boolean strategy(TickerDataDTO tickerBuy, TickerDataDTO tickerSell) {
		BigDecimal buyFee = new BigDecimal(0.1);
		BigDecimal sellFee = new BigDecimal(0.1);
		CompletableFuture<ExchangeSpecs> buy = CompletableFuture.supplyAsync(() -> getFees(tickerBuy));
		CompletableFuture<ExchangeSpecs> sell = CompletableFuture.supplyAsync(() -> getFees(tickerSell));
		CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(buy, sell);
		try {
			combinedFuture.get();
			buyFee = buy.get().getBuyFee();
			sellFee = sell.get().getSellFee();
		} catch (InterruptedException | ExecutionException e) {
			log.error(e.getMessage(), tickerBuy.getCurrencyPair().toString());

		}
		BigDecimal marginSubDiff = Calculate.diffWithMargin(tickerBuy, tickerSell, buyFee.add(sellFee));
		if (marginSubDiff.compareTo(BigDecimal.ZERO) > 0) {
			return true;

		} else {
			return false;

		}

	}

	private ExchangeSpecs getFees(TickerDataDTO ticker) {
		return SelectedExchange.getSelectedExchange().getExchange().stream()
				.filter(exchange -> exchange.getExchangeName().toLowerCase()
						.equals(ticker.getExchange().getDefaultExchangeSpecification().getExchangeName().toLowerCase()))
				.collect(Collectors.toList()).get(0);

	}

}
