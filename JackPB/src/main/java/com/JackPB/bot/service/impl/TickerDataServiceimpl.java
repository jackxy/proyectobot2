package com.JackPB.bot.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.stereotype.Service;

import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.model.TickerData;
import com.JackPB.bot.model.TickerDataDTO;
import com.JackPB.bot.service.TickerDataService;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TickerDataServiceimpl implements TickerDataService {
	@Override

	public TickerDataDTO findTickerData(ActivatedExchange activatedExchange, CurrencyPair currencyPair) {
		TickerData tickerData = this.getTickerData(activatedExchange.getExchange(), currencyPair);
		TickerDataDTO tickerDataTrading = new TickerDataDTO(tickerData, BigDecimal.ZERO, BigDecimal.ZERO);
		return tickerDataTrading;

	}

	private TickerData getTickerData(Exchange exchange, CurrencyPair currencyPair) {
		return Try.of(() -> exchange.getMarketDataService().getTicker(currencyPair)).map(result -> {
			log.info("**** " + result.toString());
			Optional<BigDecimal> bidOptional = Optional.of(result.getBid());
			Optional<BigDecimal> askOptional = Optional.of(result.getAsk());
			Optional<BigDecimal> lastOptional = Optional.of(result.getLast());
			return new TickerData(currencyPair, exchange, bidOptional.orElse(BigDecimal.ZERO),
					askOptional.orElse(BigDecimal.ZERO), lastOptional.orElse(BigDecimal.ZERO));
		}).getOrElse(new TickerData(currencyPair, exchange, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));

	}

	/**
	 * Encuentre el articulo dentro de una lista con la ASK mas baja (mejor compra)
	 * y BID mas alta (mejor venta)
	 * 
	 * @param tickersData una lista de TickerData
	 * @return Tuple2<TickerData, TickerData> lowAsk, highBid
	 */

	@Override
	public Tuple2<TickerDataDTO, TickerDataDTO> findLowHighPrice(ArrayList<TickerDataDTO> tickersData) {
		TickerDataDTO tickerBuy = this.lowAskFinder(tickersData);
		TickerDataDTO tickerSell = this.highBidFinder(tickersData, tickerBuy);
		log.info("COMPRAR EN" + tickerBuy.getExchange().getDefaultExchangeSpecification().getExchangeName() + " A "
				+ tickerBuy.getAsk().toString());
		log.info("COMPRAR EN" + tickerSell.getExchange().getDefaultExchangeSpecification().getExchangeName() + " A "
				+ tickerBuy.getAsk().toString());

		return Tuple.of(tickerBuy, tickerSell);
	}

	private TickerDataDTO lowAskFinder(ArrayList<TickerDataDTO> tickersData) {
		return tickersData.stream().min(Comparator.comparing(TickerData::getAsk))
				.orElse(new TickerDataDTO(new TickerData(null, null, null, null, null)));

	}

	private TickerDataDTO highBidFinder(ArrayList<TickerDataDTO> tickersData, TickerDataDTO tickerBuy) {
		return tickersData.stream().max(Comparator.comparing(TickerData::getBid))
				.orElse(new TickerDataDTO(new TickerData(null, null, null, null, null)));
	}
}
