package com.JackPB.bot.service;

import java.util.ArrayList;

import org.knowm.xchange.currency.CurrencyPair;

import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.model.TickerDataDTO;

import io.vavr.Tuple2;

public interface TickerDataService {

	TickerDataDTO findTickerData(ActivatedExchange activatedExchange, CurrencyPair currencyPair);

	Tuple2<TickerDataDTO, TickerDataDTO> findLowHighPrice(ArrayList<TickerDataDTO> tickersData);

}
