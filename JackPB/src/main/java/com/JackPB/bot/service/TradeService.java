package com.JackPB.bot.service;

import com.JackPB.bot.model.TickerDataDTO;

public interface TradeService {

	Boolean canTrade(TickerDataDTO tickerBuy, TickerDataDTO tickerSell);

}
