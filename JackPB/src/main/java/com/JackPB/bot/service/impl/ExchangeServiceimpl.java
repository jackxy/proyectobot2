package com.JackPB.bot.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.model.TickerDataDTO;
import com.JackPB.bot.repository.ExchangeRepository;
import com.JackPB.bot.service.ExchangeService;
import com.JackPB.bot.service.TickerDataService;

@Service
public class ExchangeServiceimpl implements ExchangeService {

	@Autowired
	ExchangeRepository exchangeRepository;
	@Autowired
	TickerDataService tickerDataService;

	@Override

	public List<ActiveExchange> findByActiveExchange() {
		return this.exchangeRepository.findByActiveTrue();
	}

	@Override

	public ArrayList<ActivatedExchange> getActivatedExchanges(ArrayList<ExchangeSpecs> selectedExchanges) {
		return selectedExchanges.stream().map(selecExchange -> this.getActiveExchange(selecExchange))
				.collect(Collectors.toCollection(ArrayList::new));

	}
	@Override

	public ArrayList<TickerDataDTO> getAllTickerData(ArrayList<ActivatedExchange> activatedExchanges,
			CurrencyPair currencyPair, BigDecimal amountValueBase) {
		ArrayList<TickerDataDTO> tickersData = new ArrayList<TickerDataDTO>();
		activatedExchanges.parallelStream().forEach(activatedExchange -> {
			TickerDataDTO tickerData;
			try {

				tickerData = this.tickerDataService.findTickerData(activatedExchange, currencyPair);
				if (null != tickerData) {
					tickerData.add(tickerData);
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		});
		return tickersData;
	}

	private ActivatedExchange getActiveExchange(ExchangeSpecs exchangeSpecs) {
		Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exchangeSpecs.getSetupedExchange());
		return ActivatedExchange.builder().activated(true).exchange(exchange).realMode(false).build();
	}

}
