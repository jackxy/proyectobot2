package com.JackPB.bot.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.knowm.xchange.currency.CurrencyPair;

import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.model.TickerDataDTO;

public interface ExchangeService {

	List<ActiveExchange> findByActiveExchange();

	ArrayList<ActivatedExchange> getActivatedExchanges(ArrayList<ExchangeSpecs> selectedExchanges);

	ArrayList<TickerDataDTO> getAllTickerData(ArrayList<ActivatedExchange> activatedExchanges,
			CurrencyPair currencyPair, BigDecimal amountValueBase);

}
